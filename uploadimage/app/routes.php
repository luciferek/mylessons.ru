<?php
	$app->get('/', function ($request, $response) use ($container) {
		return $container->view->render($response, 'home.twig');
	});