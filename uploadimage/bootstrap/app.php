<?php
	require '../app/config.php';

	$app = new \Slim\App($config);

	$container = $app->getContainer();

	$container['view'] = function ($container) {
		$view = new \Slim\Views\Twig(realpath('../resources/view/'), [
				'cache' => false
		]);
		//get extension 
		$view->addExtension(new \Slim\Views\TwigExtension(
				$container->router,
				$container->request->getUri()
		));

		return $view;
	};
 
	require '../app/routes.php';

	$app->run();