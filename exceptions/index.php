<?php 
declare(strict_types=1);
class FileException extends Exception {}
class MySqlException extends Exception {}
/**
* This is test Exception capabilities
*/
class ExceptionTester
{
	// file resource 
	protected $file; 
	
	/*
	 * @param $file_name string name file
	 * @return null 
	 */
	public function __construct(string $file_name)
	{
		if (file_exists($file_name)) {
			if (is_writeable($file_name)) {
				$file = fopen($file_name, '+r');
				if ($file) {
					$this->file = $file;
				} else {
					throw new FileException("Error openning file..", 1);
				}
			} else {
				throw new FileException('File is not writeable..', 3);
			}
		} else {
			throw new FileException("File not exists..", 2);
		}
	}

	/*
	 * write file 
	 * @param $to_file string
	 * @return true | false 
	 */
	public function writeFile(string $to_file)
	{
		if ($this->file) {
			if (!fwrite($this->file, $to_file)) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/*
	 * close file 
	 */
	private function __destruct() 
	{
		if ($this->file) {
			fclose($this->file);
		}
	}

}

// next user code
try {
	
	$file = new ExceptionTester('file.txt');
	$file->writeFile('hello world');
	
} catch (FileException $fileE) {
	echo $fileE->getMessage();
} catch (MySqlException $mysqle) {
	
}	catch ( Exception $e) {
	
} 
