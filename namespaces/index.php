<?php 
	
	require 'Class1.php';
	require 'Class2.php';

	use \Class1\Class1;
	use \Class2\Class2;

	try {

		$class1 = new Class1;
		$class2 = new Class2;
		$class1->heyMr();
		$class2->helloWorld();

	} catch (ExceptionClass1 $e1) {
		$e1->getMessage();
	} catch (ExceptionClass2 $e2) {
		$e2->getMessage();
	}