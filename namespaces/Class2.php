<?php 
namespace Class2;

/**
* Class which show 'Hello world!'
*/
class Class2 
{
	/*
	 * Show hello world
	 * @return null 
	 */
	public function helloWorld()
	{
		echo 'Hello world';
	}
}