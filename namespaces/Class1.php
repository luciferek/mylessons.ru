<?php 
namespace Class1;

/**
* Class which will be show 'Hey Mr.'
*/
class Class1
{
	/*
	 * show heyMr.
	 * @return null
	 */
	public function heyMr()
	{
		echo 'heyMr.';
	}
}

