<?php 
namespace \newnamespace\Class3;

/**
* Class which will be show us 'Random number'
*/
class Class3
{
	public function getRandomNumber() 
	{
		echo 'Random Number :' . random(1, 4) . '<br>';
		echo __NAMESPACE__;
	}
}