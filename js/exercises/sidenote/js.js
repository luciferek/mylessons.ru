var counter = false;
function positionAt (anchor, position, elem) {
	if (counter == true) {
		elem.style.left = elem.style.right = elem.style.top = elem.style.bottom = null;
		elem.className = 'note';
	}
	var widthAnchor = anchor.offsetWidth;
	var heightAnchor = anchor.offsetHeight;
	switch (position) {
		case 'right':
			elem.style.left = widthAnchor + 'px';
			elem.classList.add('note-right');
		break;
		case 'left': 
			elem.style.right = widthAnchor + 'px';
			elem.classList.add('note-left');
		break;
		case 'top':
			elem.style.bottom = heightAnchor + 'px';
			elem.classList.add('note-top');
		break;
		case 'bottom':
			elem.style.top = heightAnchor + 'px';
			elem.classList.add('note-bottom');
		break;
	}
	anchor.appendChild(elem);
	counter = true;
}

// find anchor
var anchor = document.getElementsByTagName('blockquote')[0];

// element construction 
var element = document.createElement('div');
element.className = 'note';
element.innerHTML = 'any note';

// for buttons
var buttons = document.getElementsByTagName('button');
for (var i = 0; buttons.length > i; i++) {
	buttons[i].addEventListener(
		'click', 
		function() {
			var valueId = this.getAttribute('id');
			positionAt(anchor, valueId, element );
		}
	);
}