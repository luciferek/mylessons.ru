<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login form with purecss</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:900" rel="stylesheet">
	<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.1/build/pure-min.css">
	<link rel="stylesheet" href="https://necolas.github.io/normalize.css/latest/normalize.css">
	<link rel="stylesheet" href="css/style.min.css">
</head>
<body>
	<main>
		<h1>Willy - Willy</h1>
		<p>hello my dear friend, your welcome on my test login form, it is below...</p>

		<form action="index.php" class="pure-form pure-form-stacked" method="post">
			<fieldest>
			
				<label for="email">Email:</label>
				<input type="email" id="email" placeholder="email@email.com">

				<label for="password">Password:</label>
				<input type="password" id="password" placeholder="password">

				<label for="state">State:</label>
				<select name="" id="state">
					<option value="">A1</option>
					<option value="">A2</option>
					<option value="">A3</option>
				</select>

				<label for="remember" class="pure-checkbox">
					<input type="checkbox" id="remember"> Remember me..
				</label>
				
				<button class="pure-botton pure-botton-primary" type="submit">Sign in</button>
			</fieldest>
		</form>
		<?php 
			print_r($_POST);
		?>
	</main>
</body>
</html>