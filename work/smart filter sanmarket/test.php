<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
  'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
  'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

// IHFB functional start 
//
// properties:
// array with full properties 
$full_properties = array();

// количество полных элементов
$count_full_properties = 0;

// количество столбцов
$count_const_column = 4;

// short items 
$items = $arResult['ITEMS'];
// если пустое, то пропускаем, 
// если нет, то записываем
foreach ($items as $item) {
	if(empty($item['VALUES'])) {
		continue;
	} else {
		++$count_full_properties;
		$full_properties[] = $item;
	}
}

// сколько свойств в столбце
// floor - округление к меньшему
$count_in_column = floor( $count_full_properties / $count_const_column );

//номер свойства 
$property_number = 0;

// текущий столбец
$current_column = 0;

// отсортированный массив
$main[1] = $main[2] = $main[3] = $main[4] = array();

// usage var:
////////////////////
// $full_properties
// $property_number
// $count_in_column
// $count_const_column
// $current_column
// $main
////////////////////
foreach ($full_properties as $key => $property) {
	// считаем элементы
	++$property_number;

	// если свойство не "влазеет" в нашу "таблицу" =>
	// на предыдущей итерации обнулилась $current_column -
	// т.к. номер свойства == общему количеству свойств в нашей таблице
	// то переносим указатель на первый столбец
	// при следующей итерации мы попадем сюда опять.. из-за 
	// $property_number
	if ( ceil($property_number / $count_in_column ) > $count_const_column) {
		++$current_column;
	} else {
		$current_column = ceil($property_number / $count_in_column);
	}

	// если свойство с полем ввода, то "кладем" его вверх массива
	if ( $property["PROPERTY_TYPE"] = 'N' ) {
		array_unshift($main[$current_column], $property);
	} else {
		array_push($main[$current_column], $property[$key]);
	}

	//	если property_number == $count_in_column * $count_const_column 
	//	то обнулим столбец 
	if ( $property_number == $count_in_column * $count_const_column) {
		$current_column = 0;
	}
}

?>