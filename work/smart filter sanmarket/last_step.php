	<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
		/** @var array $arParams */
		/** @var array $arResult */
		/** @global CMain $APPLICATION */
		/** @global CUser $USER */
		/** @global CDatabase $DB */
		/** @var CBitrixComponentTemplate $this */
		/** @var string $templateName */
		/** @var string $templateFile */
		/** @var string $templateFolder */
		/** @var string $componentPath */
		/** @var CBitrixComponent $component */
		$this->setFrameMode(true);

		$templateData = array(
			'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
			'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
		);
			// Start funtional IHFB  
			// properties:
			// array with full properties 
			$full_properties = array();

			// количество полных элементов с полями ввода
			$count_n_properties = 0;
			// количество других элементов 
			$count_another_properties = 0;

			// количество столбцов
			$count_const_column = 4;

			// short items 
			$items = $arResult['ITEMS'];

			// если пустое, то пропускаем, 
			// если нет, то записываем
			foreach ($items as $key => $item) {
				if(empty($item['VALUES'])) {
					continue;
				} else {
					if($item['PROPERTY_TYPE'] == 'N') {
						++$count_n_properties;
						if ($key == '77') {
							array_unshift($n_properties, $item);
						} else {
							$n_properties[] = $item;
						}
					} else {
						++$count_another_properties;
						$another_properties[] = $item;
					}
				}
			}

			// сколько свойств в столбце
			// floor - округление к меньшему
			$count_in_column = (int)floor( count( array_merge($another_properties, $n_properties)) / $count_const_column );

			//номер свойства 
			$property_number = 0;

			// текущий столбец
			$current_column = 1;


			// отсортированный массив
			$main[1] = $main[2] = $main[3] = $main[4] = array();

			foreach ($n_properties as $key => $property) {
				// считаем элементы
				++$property_number;

				// если свойство с полем ввода, то "кладем" его вверх массива
				array_push($main[$current_column], $property);

				//	если property_number == $count_in_column * $count_const_column 
				//	то обнулим столбец
				if ( is_int($property_number / $count_const_column)) {
					$current_column = 1;
				} else {
					++$current_column;
				}
			}

			foreach ($another_properties as $key => $property) {
				//считаем элементы
				++$property_number;

				array_push($main[$current_column], $property);

				if ( is_int($property_number / $count_const_column) ) {
					$current_column = 1;
				} else {
					++$current_column;
				}
			}

			$main = array_reverse($main);





		// End functional IHFB
	?>


	<?
	// I don't know what is it
	function get_position($arr, $key)
		{     
			$position = 1;          
			foreach ($arr as $val => $arrCont)
				{
					if ($key == $val)
						{
							$position = 1;
						}
					else
						{
							$position = $position +1;
						}
				}
			return $position;
		}
	?>

	<!-- start bx filter horizontal -->
	<div class="bx_filter_horizontal <?=$templateData["TEMPLATE_CLASS"]?>">
		<!-- <div class="h3">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
				<i class="fa fa-cog font-18"></i> 
				<span class="strong">ПОДБОР ПО ПАРАМЕТРАМ</span></a> 
		</div> -->

		<!-- start fucking collapse -->
		<div id="collapseTwo" class="collapse fltr">
			<div class="bx_filter_section m4">
				<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			
					<?foreach($arResult["HIDDEN"] as $arItem):?>
						<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
					<?endforeach;?>

					<!-- fucking wrapper -->
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<?php
						// количество свойств
						$count_item = 0;
						// стобцы
						$count_column = 1;
						foreach ($main as $id_column => $column): ?>
							<?php //свойства ?>
							<?php foreach ($column as $id => $property): ?>
								<div class="row">
									<?if($property['PROPERTY_TYPE'] == "N"):?>
										<?if (false and !$property["VALUES"]["MIN"]["VALUE"] ||
																		!$property["VALUES"]["MAX"]["VALUE"] || 
																		 $property["VALUES"]["MIN"]["VALUE"] == $property["VALUES"]["MAX"]["VALUE"])
												continue;?>
										<div class="bx_filter_container col-xs-12 col-sm-6 col-md-4 col-lg-4 <?php echo $property['ID'] == '77' ? 'price-property-filter' : '';?>">
											<span class="bx_filter_container_title propt-n-title">
												<span><?=$property["NAME"]?></span>
											</span>
											<div class="bx_filter_param_area">
												<div class="bx_filter_param_area_block">
													<div class="bx_input_container">
													<input
														class="min-price"
														type="text"
														name="<?echo $property["VALUES"]["MIN"]["CONTROL_NAME"]?>"
														id="<?echo $property["VALUES"]["MIN"]["CONTROL_ID"]?>"
														value="<?echo $property["VALUES"]["MIN"]["HTML_VALUE"]?>"
														size="5"
														onkeyup="smartFilter.keyup(this)"
													/>
													</div>
												</div>
												<div class="bx_filter_param_area_block">
													<div class="bx_input_container">
														<input
														class="max-price"
														type="text"
														name="<?echo $property["VALUES"]["MAX"]["CONTROL_NAME"]?>"
														id="<?echo $property["VALUES"]["MAX"]["CONTROL_ID"]?>"
														value="<?echo $property["VALUES"]["MAX"]["HTML_VALUE"]?>"
														size="5"
														onkeyup="smartFilter.keyup(this)"/>
													</div>
												</div>
												<div style="clear: both;"></div>
											</div>
											<div class="clearfix"></div>
											<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
												<div class="bx_ui_slider_range" style="left: 0; right: 0%;"  id="drag_tracker_<?=$key?>"></div>
												<a class="bx_ui_slider_handle left"  href="javascript:void(0)" style="left:0;" id="left_slider_<?=$key?>"></a>
												<a class="bx_ui_slider_handle right" href="javascript:void(0)" style="right:0%;" id="right_slider_<?=$key?>"></a>
											</div>
											<div class="bx_filter_param_area">
												<div class="bx_filter_param_area_block" id="curMinPrice_<?=$key?>"><?=number_format($property["VALUES"]["MIN"]["VALUE"], 0, ' ', ' ');?></div>
												<div class="bx_filter_param_area_block" id="curMaxPrice_<?=$key?>"><?=number_format($property["VALUES"]["MAX"]["VALUE"], 0, ' ', ' ');?></div>
												<div style="clear: both;"></div>
											</div>
										</div>
										<?
										$arJsParams = array(
											"leftSlider" => 'left_slider_'.$key,
											"rightSlider" => 'right_slider_'.$key,
											"tracker" => "drag_tracker_".$key,
											"trackerWrap" => "drag_track_".$key,
											"minInputId" => $property["VALUES"]["MIN"]["CONTROL_ID"],
											"maxInputId" => $property["VALUES"]["MAX"]["CONTROL_ID"],
											"minPrice" => $property["VALUES"]["MIN"]["VALUE"],
											"maxPrice" => $property["VALUES"]["MAX"]["VALUE"],
											"curMinPrice" => $property["VALUES"]["MIN"]["HTML_VALUE"],
											"curMaxPrice" => $property["VALUES"]["MAX"]["HTML_VALUE"],
											"precision" => 0
										);
										?>
										<script type="text/javascript" defer="defer">
											BX.ready(function(){
												var trackBar<?=$key?> = new BX.Iblock.SmartFilter.Horizontal(<?=CUtil::PhpToJSObject($arJsParams)?>);
											});
										</script>
									<?elseif(	!empty($property["VALUES"]) && 
														!isset($property["PRICE"]) && 
														$property["CODE"] =="CML2_MANUFACTURER"):?>	
										<div class="bx_filter_container prop_<?=$property["ID"];?> col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="title-property">
												<span class="bx_filter_container_title">
													<?=$property["NAME"]?>
												</span>
												<span></span>
											</div>
											<div class="full-title-property" style="display:none">
												<span class="bx_filter_container_title">
													<?=$property["NAME"]?>
												</span>
												<span></span>
											</div>
											<div class="clearfix"></div>
											<div class="bx_filter_block in-b-filtr">
												<div class="small-block">
													<?foreach(array_reverse($property["VALUES"]) as $val => $ar):?>               
														<span class="clearfix">
															<input
																type="checkbox"
																value="<?echo $ar["HTML_VALUE"]?>"
																name="<?echo $ar["CONTROL_NAME"]?>"
																id="<?echo $ar["CONTROL_ID"]?>"
																<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
																onclick="smartFilter.click(this)"
															/>
															<label for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?></label>
														</span>       
													<?endforeach;?>
												</div>
											</div>
										</div>
									<?elseif( !empty($property["VALUES"]) && 
													 !isset($property["PRICE"]) && 
													 $property["CODE"] !="CML2_MANUFACTURER" && 
													 $property["PROPERTY_TYPE"] != "N"):?>
										<div class="bx_filter_container prop_<?=$property["ID"];?> col-xs-12 col-sm-6 col-md-4 col-lg-4">
											<div class="title-property">
												<span class="bx_filter_container_title">
													<?=$property["NAME"]?>
												</span>
												<span></span>
											</div>
											<div class="full-title-property" style="display:none">
												<span class="bx_filter_container_title">
													<?=$property["NAME"]?>
												</span>
												<span></span>
											</div>
											<div class="clearfix"></div>
											<div class="bx_filter_block in-b-filtr" id="collapse_<?=$property["ID"];?>">
												<div class="small-block">
													<?foreach(array_reverse($property["VALUES"]) as $val => $ar):?>               
														<span class="clearfix">
															<input
																type="checkbox"
																value="<?echo $ar["HTML_VALUE"]?>"
																name="<?echo $ar["CONTROL_NAME"]?>"
																id="<?echo $ar["CONTROL_ID"]?>"
																<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
																onclick="smartFilter.click(this)"
															/>
															<label for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?></label>
														</span> 
														<?
														$pos = get_position($property["VALUES"], $val);           
														?>
														<?if($pos % 8 == 0):?></div><div class="small-block"><?endif;?>
													<?endforeach;?>
												</div>
											</div>
										</div>
									<?endif?>
								</div>
								<!-- end row -->
								<div class="clearfix"></div>
							<?php endforeach; ?>
							<?php ++$count_item;?>
							<?php echo $count_column == $count_const_column ? '</div>' : '</div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">';?>
							<?php ++$count_column; ?>
						<?php endforeach ?>
						</div>
					<!-- end fucking wrapper -->

					<div class="line-15-two"></div>
					<div class="bx_filter_control_section">
						<span class="icon"></span>
						<input 	class="bx_filter_search_button" 
										type="submit" 
										id="set_filter" 
										name="set_filter" 
										value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />
						<input 	class="bx_filter_search_button" 
										type="submit" 
										id="del_filter" 
										name="del_filter" 
										value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />
			
						<div class="bx_filter_popup_result" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;top: 75px;left: 25px;right: 25px;">
							<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
						<a style="display:none"> href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
							<!--<span class="ecke"></span>-->
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
				<div style="clear: both;"></div>
				<div class="shadow-bx-filter-section"></div>
			</div>
			<div class="more-search-form">
				Расширинный поиск
			</div>
			<div class="more-search-form-fullheight" style="display: none;">
				Расширенный поиск
			</div>
			<!-- end bx_filter_section -->
		</div>
		<!-- end fucking collapse -->
	</div>
	<!-- end bx filter horizontal -->


	<script>
		var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
		$( document ).ready(function () {
			if ( $('.sort-catalog-section').size() > 0 ) {
				$('.sort-catalog-section').css('display', 'none');
			}

			$('.title-property').click(function (){
				var title_property = this;
				var height_container = ($(title_property.closest('.bx_filter_container')).find('input').size() * 22) + 45;
				var container = $(title_property).closest('.bx_filter_container');
				$(container).animate({ height:height_container + 'px'},
															'slow',
															function() {
																$(title_property).css('display', 'none');
																$(title_property).siblings('.full-title-property').css('display', 'block');;
															});
			});

			$('.full-title-property').click(function (){
				var title_property = this;
				var container = $(title_property).closest('.bx_filter_container');
				$(container).animate({ height:38},
															'slow',
															function() {
																$(title_property).css('display', 'none');
																$(title_property).siblings('.title-property').css('display', 'block');
															});
			});


			var height_form_smartfilter = $('form.smartfilter').outerHeight();
			$('.more-search-form').click(function () {
				$('.bx_filter_section').animate({
					height: height_form_smartfilter,
					},
					'slow', function() {
						$('.bx_filter_section').css({
							'overflow': 'hidden',
							'height':'100%'
						});
						$('.shadow-bx-filter-section').css('display', 'none');
						$('.more-search-form-fullheight').css('display', 'block');
						$('more-search-form').css('display', 'none');
					}
				);	
			});

			$('.more-search-form-fullheight').click(function () {
				$('.bx_filter_section').animate({
					height: '145px',
					},
					'slow', function() {
						$('.bx_filter_section').css({
							'overflow': 'hidden',
						});
						$('.shadow-bx-filter-section').css('display', 'block');
						$('.more-search-form-fullheight').css('display', 'none');
						$('more-search-form').css('display', 'block');
					}
				);	
			});

		});
	</script>



	<!--
		
		8 8888888888   b.             8 8 888888888o.
		8 8888         888o.          8 8 8888    `^888.
		8 8888         Y88888o.       8 8 8888        `88.
		8 8888         .`Y888888o.    8 8 8888         `88
		8 888888888888 8o. `Y888888o. 8 8 8888          88
		8 8888         8`Y8o. `Y88888o8 8 8888          88
		8 8888         8   `Y8o. `Y8888 8 8888         ,88
		8 8888         8      `Y8o. `Y8 8 8888        ,88'
		8 8888         8         `Y8o.` 8 8888    ,o88P'
		8 888888888888 8            `Yo 8 888888888P'
	 -->
