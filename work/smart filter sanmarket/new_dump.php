<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
  'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
  'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

// IHFB functional start 
//
// properties:
// array with full properties 
$full_properties = array();

// количество полных элементов
$count_full_properties = 0;

// количество столбцов
$count_const_column = 4;

// short items 
$items = $arResult['ITEMS'];

// если пустое, то пропускаем, 
// если нет, то записываем
foreach ($items as $item) {
	if(empty($item['VALUES'])) {
		continue;
	} else {
		++$count_full_properties;
		$full_properties[] = $item;
	}
}

// сколько свойств в столбце
// floor - округление к меньшему
$count_in_column = floor( $count_full_properties / $count_const_column );

//номер свойства 
$property_number = 0;

// текущий столбец
$current_column = 0;


// отсортированный массив
$main[1] = $main[2] = $main[3] = $main[4] = array();


// usage var:
////////////////////
// $full_properties
// $property_number
// $count_in_column
// $count_const_column
// $current_column
// $main
////////////////////
foreach ($full_properties as $key => $property) {
	// считаем элементы
	++$property_number;

	// если свойство не "влазеет" в нашу "таблицу" =>
	// на предыдущей итерации обнулилась $current_column -
	// т.к. номер свойства == общему количеству свойств в нашей таблице
	// то переносим указатель на первый столбец
	// при следующей итерации мы попадем сюда опять.. из-за 
	// $property_number
	if ( ceil($property_number / $count_in_column ) > $count_const_column) {
		++$current_column;
	} else {
		$current_column = ceil($property_number / $count_in_column);
	}


	// если свойство с полем ввода, то "кладем" его вверх массива
	if ( $property["PROPERTY_TYPE"] == 'N' ) {
		array_unshift($main[$current_column], $property);
	} else {
		array_push($main[$current_column], $property);
	}

	//	если property_number == $count_in_column * $count_const_column 
	//	то обнулим столбец 
	if ( $property_number == $count_in_column * $count_const_column) {
		$current_column = 0;
	}
}

//echo "<pre>";
//print_r($main);
//echo "</pre>";


?>

<?
function get_position($arr, $key)
	{     
		$position = 1;          
		foreach ($arr as $val => $arrCont)
			{
				if ($key == $val)
					{
						$position = 1;
					}
				else
					{
						$position = $position +1;
					}         
			}
		return $position;        
	}
?>

<div class="bx_filter_horizontal <?=$templateData["TEMPLATE_CLASS"]?>">
				
		<div class="h3">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
				<i class="fa fa-cog font-18"></i> 
				<span class="strong">ПОДБОР ПО ПАРАМЕТРАМ</span></a> 
		</div>    
		
		<div id="collapseTwo" class="collapse fltr">         

		<div class="bx_filter_section m4">   
		
			<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			
				<?foreach($arResult["HIDDEN"] as $arItem):?>
					<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
				<?endforeach;?>

				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
				

				<!-- if input  -->
				<?foreach($arResult["ITEMS"] as $key=>$arItem):?>

					<?if($arItem["PROPERTY_TYPE"] == "N"):
						if (false and !$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
							continue;
						?>
						<div class="bx_filter_container col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<span class="bx_filter_container_title"><?=$arItem["NAME"]?></span>
							<div class="bx_filter_param_area">
								<div class="bx_filter_param_area_block"><div class="bx_input_container">
									<input
										class="min-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
									/>
								</div></div>
								<div class="bx_filter_param_area_block"><div class="bx_input_container">
									<input
										class="max-price"
										type="text"
										name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
										id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
										value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
										size="5"
										onkeyup="smartFilter.keyup(this)"
									/>
								</div></div>
								<div style="clear: both;"></div>
							</div>
							<div class="bx_ui_slider_track" id="drag_track_<?=$key?>">
								<div class="bx_ui_slider_range" style="left: 0; right: 0%;"  id="drag_tracker_<?=$key?>"></div>
								<a class="bx_ui_slider_handle left"  href="javascript:void(0)" style="left:0;" id="left_slider_<?=$key?>"></a>
								<a class="bx_ui_slider_handle right" href="javascript:void(0)" style="right:0%;" id="right_slider_<?=$key?>"></a>
							</div>
							<div class="bx_filter_param_area">
								<div class="bx_filter_param_area_block" id="curMinPrice_<?=$key?>"><?=number_format($arItem["VALUES"]["MIN"]["VALUE"], 0, ' ', ' ');?></div>
								<div class="bx_filter_param_area_block" id="curMaxPrice_<?=$key?>"><?=number_format($arItem["VALUES"]["MAX"]["VALUE"], 0, ' ', ' ');?></div>
								<div style="clear: both;"></div>
							</div>
						</div>            
						<?
						$arJsParams = array(
							"leftSlider" => 'left_slider_'.$key,
							"rightSlider" => 'right_slider_'.$key,
							"tracker" => "drag_tracker_".$key,
							"trackerWrap" => "drag_track_".$key,
							"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
							"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
							"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
							"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
							"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
							"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
							"precision" => 0
						);
						?>
						<script type="text/javascript" defer="defer">
							BX.ready(function(){
								var trackBar<?=$key?> = new BX.Iblock.SmartFilter.Horizontal(<?=CUtil::PhpToJSObject($arJsParams)?>);
							});
						</script>

						<?endif;?>
				<?endforeach;?>
				<!-- if input end -->
				<!-- if manufacture  -->
				<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
						<?if(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"]) && $arItem["CODE"] =="CML2_MANUFACTURER"):?>
								<div class="bx_filter_container prop_<?=$arItem["ID"];?> col-xs-12 col-sm-6 col-md-4 col-lg-4">          
										<span class="bx_filter_container_title">
												<?=$arItem["NAME"]?>
										</span>                    
										<div class="bx_filter_block in-b-filtr">
											<div class="small-block">
												<?foreach(array_reverse($arItem["VALUES"]) as $val => $ar):?>               
													<span class="clearfix">
														<input
															type="checkbox"
															value="<?echo $ar["HTML_VALUE"]?>"
															name="<?echo $ar["CONTROL_NAME"]?>"
															id="<?echo $ar["CONTROL_ID"]?>"
															<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
															onclick="smartFilter.click(this)"
														/>
														<label for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?></label>
													</span>       
													
												<?endforeach;?>
											</div>
										</div>
							</div>
						<?endif;?>
				<?endforeach;?>
				<!-- end if manufacture -->
				<!-- another properties -->
				<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
						
					<?if(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"]) && $arItem["CODE"] !="CML2_MANUFACTURER" && $arItem["PROPERTY_TYPE"] != "N"):?>           

					<div class="bx_filter_container prop_<?=$arItem["ID"];?> col-xs-12 col-sm-6 col-md-4 col-lg-4">
					
						<span class="bx_filter_container_title">
							<?=$arItem["NAME"]?>
						</span>
						
						<div class="bx_filter_block in-b-filtr" id="collapse_<?=$arItem["ID"];?>">
							<div class="small-block">
								<?foreach(array_reverse($arItem["VALUES"]) as $val => $ar):?>               
									<span class="clearfix">
										<input
											type="checkbox"
											value="<?echo $ar["HTML_VALUE"]?>"
											name="<?echo $ar["CONTROL_NAME"]?>"
											id="<?echo $ar["CONTROL_ID"]?>"
											<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
											onclick="smartFilter.click(this)"
										/>
										<label for="<?echo $ar["CONTROL_ID"]?>"><?echo $ar["VALUE"];?></label>
									</span> 
									<?
									$pos = get_position($arItem["VALUES"], $val);           
									?>
									<?if($pos % 8 == 0):?></div><div class="small-block"><?endif;?>
								<?endforeach;?>
							</div>
						</div>
					</div>
					<?endif;
					endforeach;?>
				<!-- end another properties -->
		</div>
				<div class="line-15-two"></div>
				<div class="bx_filter_control_section">
					<span class="icon"></span><input class="bx_filter_search_button" type="submit" id="set_filter" name="set_filter" value="<?=GetMessage("CT_BCSF_SET_FILTER")?>" />
					<input class="bx_filter_search_button" type="submit" id="del_filter" name="del_filter" value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>" />

					<div class="bx_filter_popup_result" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;top: 75px;left: 25px;right: 25px;">
						<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
						<a href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
						<!--<span class="ecke"></span>-->
					</div>
				</div>
			</form>
		<div style="clear: both;"></div>
</div>  
	
				</div><!--collapse-->
	 
</div>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>
